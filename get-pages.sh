#!/bin/sh

FOLDER=raw_pages

mkdir -p $FOLDER

i=0
for id in `cat ids.txt`; do
	echo $i $id
	i=$((i+1))
	wget -qO "$FOLDER/$id.html" --post-data 'snr=1_agecheck_agecheck__age-gate&ageDay=4&ageMonth=April&ageYear=1994' 'http://store.steampowered.com/agecheck/app/'$id'/'
	sleep 1
done
