import pandas
import numpy as np
from matplotlib import pyplot as plt


def run():
    # preformat_data()
    visualize()


def preformat_data():
    data = pandas.read_csv("basic.csv")
    data = data.fillna('')
    # data = data[:5]

    alltags = set()
    for i, row in data.iterrows():
        for tag in row['Tags'].split(';'):
            alltags.add(tag)
    print(alltags)
    print(len(alltags))

    # 334 Tags
    # Genres like '
    # Weird & Funny: NSFW, 6DOF, FMV, Illuminati, Walking Simulator (152), Spectacle fighter, Intentionally Awkward Controls
    columns = ["ID", "Name"] + list(alltags)
    newdata = pandas.DataFrame(columns=columns)

    for i, row in data.iterrows():
        newrow = [row["ID"], row["Name"]] + list(map(lambda tag: tag in row['Tags'].split(';'), alltags))
        newdata.loc[i] = newrow

    newdata.to_csv("tagdata.csv")


def visualize():
    data = pandas.read_csv('tagdata.csv')
    # data = data[:10]
    tagcolumns = data.columns.difference(['ID', 'Name', 'Unnamed: 0', 'Unnamed: 3'])
    tagcounts = dict(np.sum(data[tagcolumns], 0))
    sorteditems = sorted(tagcounts.items(), key=lambda x: -x[1])
    keys, val = list(zip(*sorteditems))
    # plt.bar(np.arange(len(keys)), val)
    # plt.show()

    topN = 8
    plt.bar(np.arange(topN), val[:topN])
    print(list(keys)[:topN])
    plt.xticks(np.arange(0.5, topN + 0.5), keys[:topN], rotation=90)
    plt.show(width=1024, height=1024)
    plt.hist()


if __name__ == '__main__':
    run()
