from collections import defaultdict
import pandas


def until(char, line):
    ix = line.index(char)
    return line[0:ix]

def run():
    filename = 'intermediate_format.txt'
    records = []
    record = defaultdict(list)
    i = 0
    value = ''
    valid_keys = ['ID', 'Features', 'Tags', 'Metascore', 'ReleaseDate', 'Publisher', 'Price', 'PosReviewPercentage',
                  'Genre', 'TotalReviews', 'Developer', 'Name', 'ShortDesc', 'LongDesc', 'End']
    done_records = 0
    for line in open(filename, encoding='utf-8'):
        i += 1
        # if i > 250:
        #     break

        # if ':' in line and until(':', line) not in valid_keys:
        #     print('invalid_key: {}'.format(line))

        if ':' in line and until(':', line) in valid_keys:
            line = line[:-1]
            if value != '':
                record[key].append(value)

            ix = line.index(':')
            key = line[0:ix]
            if key == 'End':
                done_records += 1
                print(done_records)
                records.append(record)
                record = defaultdict(list)
            else:
                if len(line) > ix+1:
                    value = line[ix+2:]
                    record[key].append(value)
            value = ''
        else:
            value = '{} {}'.format(value, line)

    # for record in records:
    #     print(record)

    valid_keys.remove('End')
    data = pandas.DataFrame(columns=valid_keys)
    done_records = 0
    for record in records:
        done_records += 1
        print(done_records)
        data = data.append({key: ';'.join(value) for key, value in record.items()}, ignore_index=True)

    data['Price'] = list(map(lambda x: str(x).split(';')[0], data['Price']))
    print(data)
    data.to_csv('basic.csv')

if __name__ == '__main__':
    run()