import re
import pandas
import nltk
from nltk.stem import SnowballStemmer
from collections import defaultdict
import operator


def preprocess_csv(filename):
    dehtmlizer = re.compile('<[^>]*>|\r|\n|\t')
    data = pandas.read_csv(filename)
    data.fillna('', inplace=True)
    data['LongDesc'] = map(lambda s: dehtmlizer.sub('', s.decode('ascii', 'ignore')), data['LongDesc'])
    data['ShortDesc'] = map(lambda s: s.decode('ascii', 'ignore'), data['ShortDesc'])

    data['LongDesc'] = map(lambda s: ' '.join(nltk.word_tokenize(s)), data['LongDesc'])
    data['ShortDesc'] = map(lambda s: ' '.join(nltk.word_tokenize(s)), data['ShortDesc'])

    return data


def create_ngrams(data, column, upto=1):
    dicts = [defaultdict(int) for _ in range(upto)]
    stemmer = SnowballStemmer("english")

    def get_words(line):
        words = map(stemmer.stem, str(elem).lower().split(' '))
        # words = str(line).lower().split(' ')
        words = [word for word in words if len(word) > 1]
        return words

    for elem in data[column]:
        words = get_words(elem)
        for word in words:
            dicts[0][word] += 1
        if upto >= 2:
            for bigram in nltk.bigrams(words):
                dicts[1][bigram] += 1
        if upto >= 3:
            for trigram in nltk.trigrams(words):
                dicts[2][trigram] += 1

    new_columns = []
    for i in range(upto):
        colnames = [key for key, value in dicts[i].items() if value > 50]
        print len(colnames)
        print colnames[:100]
        new_columns.append(colnames)

    new_data = pandas.DataFrame(columns=[item for sublist in new_columns for item in sublist])
    new_data.columns = map(lambda x: ' '.join(x) if type(x) is tuple else x, new_data.columns)
    print new_data.columns

    for i, elem in zip(range(data.shape[0]), data[column]):
        if i % 10 == 0:
            print '{} / {}' .format(i, data.shape[0])
        words = get_words(elem)
        colnames = new_columns[0]

        row = [1 if word in words else 0 for word in colnames]

        if upto >= 2:
            bigrams = nltk.bigrams(words)
            colnames = new_columns[1]
            row += [1 if bigram in bigrams else 0 for bigram in colnames]
        if upto >= 3:
            trigrams = nltk.trigrams(words)
            colnames = new_columns[2]
            row += [1 if trigram in trigrams else 0 for trigram in colnames]

        new_data.loc[i] = row

    othercolumns = list(data.columns)
    del othercolumns[othercolumns.index(column)]

    new_data = new_data.astype(int)
    new_data = pandas.concat([data[othercolumns], new_data], axis=1)
    return new_data


def run():
    # datafile = 'data/basic.csv'
    # data = preprocess_csv(datafile)
    # data.to_csv('data/tokenized.csv', index=None, encoding='utf-8')

    data = pandas.read_csv('data/tokenized.csv')
    ngrammed = create_ngrams(data, 'LongDesc', upto=3)
    ngrammed.to_csv('data/ngrammed.csv', index=None)

    # datafile = 'data/tokenized.csv'
    # data = pandas.read_csv(datafile)
    # data = data[['LongDesc', 'PosReviewPercentage']]
    # data.to_csv('data/for_petrbel.csv', index=None)

if __name__ == '__main__':
    run()
