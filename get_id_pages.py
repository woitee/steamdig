# -*- coding: utf-8 -*-
from selenium import webdriver
import sys
import codecs
import time

def run():
    filename = 'ids.txt'
    f = open(filename, 'w')

    driver = webdriver.Firefox()
    all_ids = []

    for i in range(1, 1452):
        driver.get('http://store.steampowered.com/search/?snr=1_4_4__12&term=#sort_by=&sort_order=0&page={0}'.format(i))
        time.sleep(10)
        xpath = '//@data-ds-appid'
        ids = list(map(lambda el: el.get_property("value"), driver.find_elements_by_xpath('//@data-ds-appid')))
        all_ids.extend(ids)
        f.write("\n".join(ids) + "\n")
        print(ids)

    driver.quit()

if __name__ == '__main__':
    run()
