import pandas
import numpy as np
import matplotlib as plt
from wordcloud import WordCloud, STOPWORDS
import re

def run():
    data = pandas.read_csv("basic.csv")
    # data = data[:2]

    puretext = ' '.join(map(lambda x: str(x), data['LongDesc'].tolist()))
    for regex in ['&[^&;]*;', 'quot;', '<[^<>*]*>']:
        puretext = re.sub(regex, ' ', puretext)

    cloud = WordCloud(width=1024, height=768, colormap="rainbow")
    cloud.generate_from_text(puretext)
    # cloud.to_image().show()
    cloud.to_image().save('longdescmap.png')


if __name__ == '__main__':
    run()