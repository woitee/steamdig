#!/bin/sh
i=0
for file in `ls raw_pages`; do
	# Filter non-games
	if ! grep '<h2>About This Game<\/h2>' "raw_pages/$file" >/dev/null 2>/dev/null; then
		continue
	fi
	echo "$file" | sed -e 's/\(.*\)\.html/ID: \1/'
	sed -ne 's@^.*<title>\(.*\) on Steam</title>.*$@Name: \1@p'\
		-e  's@^.*<meta name="Description" content="\(.*\)">.*$@ShortDesc: \1@p'\
		-e  's@^.*- \([0-9]*\)*% of the \([0-9,]*\) user reviews for this game are positive\..*</span>@PosReviewPercentage: \1\
TotalReviews: \2@p'\
		-e  's@^.*- \([0-9]*\)*% of the \([0-9,]*\) user reviews in the last 30 days are positive\..*</span>@PosReviewPercentageLast30Days: \1\
TotalReviewsLast30Days: \2@p'\
		-e  's@^.*<b>Release Date:</b> \([0-9A-Za-z, ]*\)<br>@ReleaseDate: \1@p' \
		-e  '/<div class="glance_tags popular_tags" data-appid="[0-9]*"/,/<\/div>/{
			//!s@[[:space:]]*\([0-9A-Za-z:,\.;-][0-9A-Za-z:,\.; -]*[0-9A-Za-z:,\.;-]\)[^ ].*</a>.*$@Tags: \1@p
		}' \
		-e '/href="http:\/\/store.steampowered.com\/search\/?category2=/{
			s@<a class="name" href="http://store.steampowered.com/search/?category2=[^>]*>\([^<]*\)</a>@!00deletuntilthis!Features: \1@gp
		}' \
		-e '/<div class="game_purchase_price price">/{
			N
			s@.*<div[^>]*>[[:space:]]*\([^[[:space:]]*\)[[:space:]]*</div.*@Price: \1@p
		}' \
		-e '/<b>Genre:<\/b>/{
			N
			s@.*<a[^>]*>\([^<]*\)</a>.*@Genre: \1@p
		}' \
		-e '/<b>Developer:<\/b>/{
			N
			s@.*<a[^>]*>\([^<]*\)</a>.*@Developer: \1@p
		}' \
		-e '/<b>Publisher:<\/b>/{
			N
			s@.*<a[^>]*>\([^<]*\)</a>.*@Publisher: \1@p
		}' \
		-e '/game_area_metascore/{
			N
			s@.*<span>\([^<]*\)</span>.*@Metascore: \1@p
		}' \
		-e '/<h2>About This Game<\/h2>/,/<\/div>/{
			/<h2>About This Game/s/.*/LongDesc:/
			/<\/div>/s/[\r\t ]*<\/div>//
			p
		}' \
		"raw_pages/$file" | tee test.txt |
			grep -v '^[[:space:]]*<\/div>[[:space:]]*$' |
			sed -e 's/.*!00deletuntilthis!//' \
				-e 's/^<br>//' \
				-e 's/^[\t\r ]*//'
	echo "End: "
	i=$(($i + 1))
	echo $i >&2
done